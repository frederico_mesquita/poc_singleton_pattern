package br.com.frederico_mesquita.creational.control;

import br.com.frederico_mesquita.creational.entity.AnnualMember;
import br.com.frederico_mesquita.creational.entity.IMemberFactory;
import br.com.frederico_mesquita.creational.entity.IMembership;
import br.com.frederico_mesquita.creational.entity.LifetimeMember;
import br.com.frederico_mesquita.creational.entity.SubscriptionType;
import br.com.frederico_mesquita.creational.entity.TemporaryMember;

public class MemberFactory implements IMemberFactory {
	private static MemberFactory _instance;
	
	private MemberFactory() {}
	
	public static MemberFactory getMemberFactory() {
		if(null == _instance) {
			_instance = new MemberFactory();
			System.out.println("new MemberFactory()");
		} else {
			System.out.println("same old instance");
		}
		return _instance;
	}

	@Override
	public IMembership subscribe(SubscriptionType subscriptionType, String name) {
		IMembership member = null;
		
		switch (subscriptionType) {
		case ANNUAL_MEMBER:
			member = new AnnualMember();
			break;
		case LIFETIME:
			member = new LifetimeMember();
			break;
		case TEMPORARY_MEMBER:
			member = new TemporaryMember();
			break;
		default:
			break;
		}
		
		if(null != member) {
			member.registerMember(name);	
		}
		
		return member;
	}

}
