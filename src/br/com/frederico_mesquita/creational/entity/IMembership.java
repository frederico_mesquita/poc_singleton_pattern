package br.com.frederico_mesquita.creational.entity;

public interface IMembership {
	void registerMember(String param);
	void showMember();
}
